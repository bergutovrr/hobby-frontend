import * as express from 'express';
import {Request} from 'express';
import * as React from 'react';
import * as compression from 'compression';
import {initialHandler} from './index';
import * as path from "path";


const app = express();

app.use(express.static(path.resolve('build/client')));
app.use(compression({filter: (req: Request) => req.headers['x-no-compression'] !== undefined}));

app.get('*', initialHandler);
const PORT = 3000;

app.listen(PORT, () => {
    console.log(`http://localhost:${PORT}`);
});