import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from "../shared/App";
import '../styles/style.scss';
import storeConstructor from "../shared/state/store";
import createBrowserHistory from "history/createBrowserHistory";
import RootState from "../shared/state/RootState";
import {Provider} from "react-redux";
import {ConnectedRouter} from "connected-react-router";
import getRaven from "../shared/layers/raven";

getRaven();

const history = createBrowserHistory();
const store = storeConstructor({
    history,
    initialState: window.__PRELOADED_STATE__ as RootState
});

window.store = store;

const root = document.getElementById('root');
ReactDOM.hydrate(<Provider store={store}>
    <ConnectedRouter history={history}>
        <App/>
    </ConnectedRouter>
</Provider>, root);