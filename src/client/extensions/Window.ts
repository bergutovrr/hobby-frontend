import RootState from "../../shared/state/RootState";
import Environment from "../../shared/layers/environment";
import {Store} from "redux";
import Action from "../../shared/state/actions";

declare global {
    interface Window {
        env: Environment,
        __PRELOADED_STATE__?: string | RootState,
        __REDUX_DEVTOOLS_EXTENSION__?: any,
        __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: any,
        store: Store<RootState, Action.List>
    }
}

