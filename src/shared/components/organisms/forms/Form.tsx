import * as React from "react";
import {ErrorInfo, FormEvent} from "react";
import Button, {Type} from "../../atoms/Button";

export type Inputs<T> = { [property in keyof T]?: T[property] };

interface Props<T> {
    children: (onChange: (field: keyof T, value: T[keyof T]) => any, inputs: Inputs<T>) => React.ReactNode,
    onChange?: (inputs: Inputs<T>) => any,
    onSubmit?: (inputs: Inputs<T>) => any
}


interface State<T> {
    inputs: Inputs<T>
}

export default class Form<T> extends React.Component<Props<T>, State<T>> {

    state: State<T> = {
        inputs: {}
    };

    onChange = (field: keyof T, value: T[keyof T]) => {

        const state: State<T> = this.state;
        const inputs: Inputs<T> = state.inputs;

        inputs[field] = value || "";

        if (this.props.onChange) this.props.onChange(inputs);

        this.setState({inputs: inputs});
    };

    onSubmit = (event: FormEvent<HTMLFormElement>) => {
        event.preventDefault();
        if (this.props.onSubmit) this.props.onSubmit(this.state.inputs);
        return false;
    };

    componentDidCatch(error: Error, errorInfo: ErrorInfo) {
        console.error(error, errorInfo);
    }

    render() {
        return <form action="" onSubmit={this.onSubmit}>
            {this.props.children(this.onChange, this.state.inputs)}
        </form>;
    }
}