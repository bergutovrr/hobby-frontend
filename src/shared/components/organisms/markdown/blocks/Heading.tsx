import * as React from "react";
import {HEADING} from "@blueprintjs/core/lib/cjs/common/classes";

interface Props {
    level: number,
    children: string
}

const Heading = ({children, level}: Props) => {
    const Tag = `h${level}`;
    return <Tag className={`${HEADING} mt-3 mb-2`}>{children}</Tag>
};

export default Heading;