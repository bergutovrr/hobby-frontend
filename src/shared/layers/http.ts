import axios, {AxiosRequestConfig} from 'axios';
import {Endpoint} from "./api/api";
import {loginSuccessful} from "../state/actions/authentication";
import services from "../services";
import JWTToken, {AuthJWTPayload} from "../models/JWTToken";
import HTTPHeader from "../enums/HTTPHeader";
import '../extensions/date';
import {Milliseconds} from "../enums/DateEnums";

axios.interceptors.request.use(async (request): Promise<AxiosRequestConfig> => {

    if (request.url && request.url.includes(Endpoint.TokenRefresh())) return request;

    const authorizationHeader = request.headers.common[HTTPHeader.Authorization];

    if (!authorizationHeader) return request;

    if (typeof authorizationHeader !== "string") return request;

    if (!authorizationHeader.includes("JWT")) return request;

    const token = authorizationHeader.replace("JWT ", "");
    const jwt_token = new JWTToken<AuthJWTPayload>(token);
    const expirationDate = new Date();
    expirationDate.setTime(jwt_token.payload.exp * 1000);
    const now = new Date();
    const diffInDays = expirationDate.getTime() / Milliseconds.PerDay - now.getTime() / Milliseconds.PerDay;

    if (diffInDays > 3) return request;

    try {
        const refreshResponse = await services.auth.jwt.refresh(token);
        if (refreshResponse.status === 200) window.store.dispatch(loginSuccessful(token));
    } catch (e) {

    }

    return request;
});