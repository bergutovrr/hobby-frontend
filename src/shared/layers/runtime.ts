export const debounce = <T extends (...args: T[]) => Promise<any> | any>(func: T, timeout: number = 300): T => {
    let timerID: number;
    return (async (...args: any[]) => {
        window.clearTimeout(timerID);
        timerID = window.setTimeout(() => func(...args), timeout)
    }) as T;
};


// @ts-ignore
export const safe = <T>(obj: T): T => new Proxy<T>(obj, {
    get: function (target: T, name: PropertyKey) {
        // @ts-ignore
        const result = target[name];
        if (!!result) return (result instanceof Object) ? safe(result) : result;
        return safe({});
    }
});