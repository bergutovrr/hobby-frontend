import KeyValueStore, {Hashable} from "./stores/KeyValueStore";
import InMemoryKeyValueStore from "./stores/InMemoryKeyValueStore";

type DecoratorOptions = {
    flush?: boolean,
    expire?: number
}

export default class KeyValueCache {

    store: KeyValueStore;

    constructor(store: KeyValueStore = new InMemoryKeyValueStore()) {
        this.store = store;
    }

    async get(key: Hashable) {
        try {
            return await this.store.get(key);
        } catch (e) {
            return null;
        }
    }

    async set(key: Hashable, value: any, expire = 600) {
        try {
            return await this.store.set(key, value, {expire});
        } catch (e) {
            return false;
        }
    }

    decorate = <T extends (...args: T[]) => Promise<any>>(func: T, {flush = false, expire = 600}: DecoratorOptions = {}): T => {
        return (async (...args: any[]) => {

            const keyArgs = args
                .concat(func.toString()
                    .replace(/\s/g, ''));

            if (!flush) {
                const cachedResult = await this.get(keyArgs);
                if (cachedResult) return cachedResult;
            }

            let result = func(...args);
            if (result instanceof Promise) result = await result;

            if (result !== null) await this.set(keyArgs, result, expire);

            return result;
        }) as T;
    }
}