import KeyValueStore, {Hashable, WriteOptions} from "./KeyValueStore";
import * as hash from 'object-hash';

export default class InMemoryKeyValueStore implements KeyValueStore {

    store: {
        [key: string]: {
            value: Hashable,
            expirationTime: number
        }
    } = {};

    get = async (key: Hashable): Promise<Object | number | string | null> => {
        const keyHash = hash(key, {
            unorderedArrays: true
        });

        if (!this.store.hasOwnProperty(keyHash)) return null;

        const value = this.store[keyHash];

        if (value.expirationTime < Date.now()) {
            delete this.store[keyHash];

            return null;
        }

        return value;
    };

    set = async (key: Hashable, value: any, {expire = 600}: WriteOptions = {}): Promise<boolean> => {

        const keyHash = hash(key, {
            unorderedArrays: true
        });

        this.store[keyHash] = {value: value, expirationTime: expire * 1000 + Date.now()};

        return true;
    }

}