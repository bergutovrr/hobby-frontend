import Lot from "../../../models/Lot";
import * as React from "react";
import LotCard from "./LotCard";

interface Props {
    lots: Lot[]
}

const LotList = ({lots}: Props) => {
    return <div className="row">
        {lots.map(lot => <div key={lot.id} className="col-xs-12 col-sm-12 col-md-4 col-lg-4 mt-3">
            <LotCard lot={lot}/>
        </div>)}
    </div>
};

export default LotList;