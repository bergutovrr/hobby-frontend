import * as React from "react";
import {Button, FormGroup, InputGroup, Intent, Tooltip} from "@blueprintjs/core";
import {connect} from "react-redux";
import RootState from "../../../state/RootState";
import {login} from "../../../state/actions/authentication";
import {ThunkDispatch} from "redux-thunk";
import Action from "../../../state/actions";

interface FormFields {
    username: string,
    password: string
}

interface Props {
    inAuthenticationProcess: boolean
}

interface DispatchProps {
    onLoginClick: (credentials: FormFields) => void
}

interface State {
    showPassword: boolean
    fields: FormFields
}

let SignInFormComponent = class extends React.Component<DispatchProps & Props, State> {

    state: State = {
        showPassword: false,
        fields: {
            username: "",
            password: ""
        }
    };

    handleLockClick = () => this.setState({showPassword: !this.state.showPassword});

    handleUsernameChange = (evt: React.FormEvent<HTMLInputElement>) => this.setState({
        fields: {...this.state.fields, username: (evt.target as HTMLInputElement).value}
    });

    handlePasswordChange = (evt: React.FormEvent<HTMLInputElement>) => this.setState({
        fields: {...this.state.fields, password: (evt.target as HTMLInputElement).value}
    });

    handleSubmitClick = (evt: React.FormEvent<HTMLElement>) => {
        evt.preventDefault();

        if (!this.state.fields.password || !this.state.fields.username) return;

        this.props.onLoginClick(this.state.fields);
    };

    render() {
        const {inAuthenticationProcess} = this.props;
        const {showPassword, fields: {username, password}} = this.state;

        const lockButton = <Tooltip content={`${showPassword ? "Скрыть" : "Показать"} пароль`}>
            <Button icon={showPassword ? "unlock" : "lock"} intent={Intent.WARNING} minimal
                    onClick={this.handleLockClick}/>
        </Tooltip>;

        return <form onSubmit={this.handleSubmitClick}>
            <FormGroup key="username" label="Имя пользователя"
                       helperText="Обязательное поле. Не более 150 символов. Только буквы, цифры и символы @/./+/-/_.">
                <InputGroup large required placeholder="Ваше имя пользователя" value={username}
                            onChange={this.handleUsernameChange}/>
            </FormGroup>
            <FormGroup key="password" label="Пароль">
                <InputGroup large required type={showPassword ? "text" : "password"} rightElement={lockButton}
                            value={password} onChange={this.handlePasswordChange}/>
            </FormGroup>
            <Button text="Войти" type="submit" large fill onClick={this.handleSubmitClick}
                    loading={inAuthenticationProcess}/>
        </form>
    }
};

const SignInForm = connect<Props, DispatchProps, {}, RootState>(
    state => ({inAuthenticationProcess: state.authentication.inAuthenticatingProcess}),
    (dispatch: ThunkDispatch<RootState, {}, Action.Reducible>) => ({onLoginClick: (credentials: FormFields) => dispatch(login(credentials))})
)(SignInFormComponent);

export default SignInForm;