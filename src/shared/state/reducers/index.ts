import {combineReducers} from "redux";
import RootState from "../RootState";
import authentication from "./authentication";
import isDarkTheme from "./theme";
import ignoredTags from "./ignoredTag";
import tags from './tags';
import Action from "../actions";

const reducer = combineReducers<RootState, Action.List>({
    authentication,
    isDarkTheme,
    ignoredTags,
    tags
});

export default reducer;