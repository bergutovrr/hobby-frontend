import Action from "../actions";

function isDarkTheme(state: boolean = true, action: Action.List): boolean {
    switch (action.type) {
        case Action.Type.SwitchTheme:
            return action.dark;
        default:
            return state;
    }
}

export default isDarkTheme;