import Action from './index';
import Cookie from "../../enums/Cookie";
// @ts-ignore
import * as Cookies from 'universal-cookie';

export interface SwitchThemeAction extends Action.Reducible {
    type: Action.Type.SwitchTheme,
    dark: boolean
}

export function switchTheme(dark: boolean): SwitchThemeAction {

    let cookies = new Cookies();

    cookies.set(Cookie.DarkTheme, dark, {
        path: '/'
    });

    return {type: Action.Type.SwitchTheme, dark};
}