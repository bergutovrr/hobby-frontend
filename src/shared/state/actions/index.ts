import {Action as ReduxAction} from "redux";
import {LoginFailedAction, LoginProcessStarted, LoginSuccessfulAction, LogoutAction} from "./authentication";
import {ThunkAction, ThunkDispatch} from "redux-thunk";
import RootState from "../RootState";
import {SwitchThemeAction} from "./theme";
import {
    FetchIgnoredTagsSuccessAction,
    IgnoreTagSuccessfulAction,
    RemoveIgnoredTagSuccessfulAction
} from "./ignoredTags";
import {CreateTagSuccessfulAction, FetchTagsSuccessfulAction} from "./tags";

module Action {
    export enum Type {
        SwitchTheme = "SWITCH_THEME",

        Login = "LOGIN",
        LoginSuccess = "LOGIN_SUCCESS",
        LoginFailed = "LOGIN_FAILED",
        Logout = "LOGOUT",

        IgnoreTagSuccess = "IGNORE_TAG_SUCCESS",
        RemoveIgnoredTagSuccess = "REMOVE_IGNORED_TAG_SUCCESS",
        FetchIgnoredTagsSuccess = "FETCH_IGNORE_TAGS_SUCCESS",
        FetchIgnoredTagsFailed = "FETCH_IGNORED_TAGS_FAILED",

        CreateTagSuccess = "CREATE_TAG_SUCCESS",
        FetchTagsSuccess = "FETCH_TAGS_SUCCESS"
    }

    export type Reducible = ReduxAction<Type>;
    export type Delayed = ThunkAction<Promise<Reducible | void>, RootState, {}, Reducible>;
    export type DelayedDispatch = ThunkDispatch<RootState, {}, Reducible>;

    export type AuthActions =
        LogoutAction |
        LoginSuccessfulAction |
        LoginFailedAction |
        LoginProcessStarted;

    export type IgnoredTagActions =
        FetchIgnoredTagsSuccessAction |
        RemoveIgnoredTagSuccessfulAction |
        IgnoreTagSuccessfulAction;

    export type TagActions =
        CreateTagSuccessfulAction |
        FetchTagsSuccessfulAction;

    export type List =
        AuthActions |
        SwitchThemeAction |
        IgnoredTagActions |
        TagActions;
}


export default Action;