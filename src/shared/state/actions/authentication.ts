import Action from './index';
import JWTToken, {AuthJWTPayload} from "../../models/JWTToken";
// @ts-ignore
import * as Cookies from 'universal-cookie';
import Cookie from "../../enums/Cookie";
import {Dispatch} from "redux";
import services from "../../services";
import axios from 'axios';
import HTTPHeader from "../../enums/HTTPHeader";

interface LoginParams {
    username: string,
    password: string
}

export type LoginAction = Action.Delayed;

export const login = (params: LoginParams): LoginAction => async (dispatch: Dispatch): Promise<void> => {

    dispatch(loginProcessStarted());

    try {
        const response = await services.auth.jwt.obtain(params);
        if (response.status === 200) dispatch(loginSuccessful(response.data.token));
        else dispatch(loginFailed(new Error("Неправильный пароль")));
    } catch (e) {
        console.log(e);
        dispatch(loginFailed(e));
    }
};

export interface LoginProcessStarted extends Action.Reducible {
    type: Action.Type.Login
}

const loginProcessStarted = (): LoginProcessStarted => ({type: Action.Type.Login});

export interface LoginSuccessfulAction extends Action.Reducible {
    type: Action.Type.LoginSuccess,
    authenticatedUser: AuthJWTPayload & { token: string }
}

export function loginSuccessful(token: string): LoginSuccessfulAction {
    const jwt_token = new JWTToken<AuthJWTPayload>(token);
    const user: AuthJWTPayload & { token: string } = {...jwt_token.payload, token: jwt_token.token};
    let cookies = new Cookies();

    cookies.set(Cookie.AuthToken, user.token, {
        path: '/'
    });

    axios.defaults.headers.common[HTTPHeader.Authorization] = `JWT ${user.token}`;

    return {type: Action.Type.LoginSuccess, authenticatedUser: user}
}

export interface LoginFailedAction extends Action.Reducible {
    type: Action.Type.LoginFailed,
    error: Error
}

const loginFailed = (error: Error): LoginFailedAction => ({type: Action.Type.LoginFailed, error});

export interface LogoutAction extends Action.Reducible {
    type: Action.Type.Logout
}

export function logout(): LogoutAction {
    let cookies = new Cookies();
    cookies.remove(Cookie.AuthToken);
    delete axios.defaults.headers.common[HTTPHeader.Authorization];
    return {type: Action.Type.Logout};
}