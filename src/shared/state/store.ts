import {applyMiddleware, compose, createStore} from "redux";
import reducer from './reducers/index';
import RootState from "./RootState";
import {History} from "history";
import {connectRouter, routerMiddleware} from "connected-react-router";
import thunk from "redux-thunk";

declare let window: Window;

const composeEnhancers = (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

interface Params {
    history: History,
    initialState: RootState,
}

const storeConstructor = ({history, initialState}: Params) => createStore(
    connectRouter(history)(reducer),
    initialState,
    composeEnhancers(applyMiddleware(routerMiddleware(history), thunk))
);

export default storeConstructor;