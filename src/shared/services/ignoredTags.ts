import IgnoredTag from "../models/IgnoredTag";
import axios from 'axios';
import getEndpoint, {Endpoint} from "../layers/api/api";

export const list = () => axios.get<IgnoredTag[]>(getEndpoint(Endpoint.IgnoredTags()));

export const create = (tag: number) => axios.post<IgnoredTag>(getEndpoint(Endpoint.IgnoredTags()), {tag}, {
    withCredentials: true
});

export const remove = (id: number) => axios.delete(getEndpoint(Endpoint.IgnoredTagDetail(id)), {
    withCredentials: true
});