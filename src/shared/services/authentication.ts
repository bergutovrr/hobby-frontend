import {default as getEndpoint, Endpoint} from "../layers/api/api";
import axios from 'axios';

interface LoginParams {
    username: string,
    password: string
}

export const jwt = {
    obtain: (params: LoginParams) => axios.post(getEndpoint(Endpoint.TokenObtain()), params),
    refresh: (token: string) => axios.post(getEndpoint(Endpoint.TokenRefresh()), {token})
};