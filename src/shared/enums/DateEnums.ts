export enum IntervalUnits {
    Milliseconds = 1000,
    Seconds = 60,
    Minutes = 60,
    Hours = 24
}

export enum Milliseconds {
    PerDay = IntervalUnits.Milliseconds * IntervalUnits.Seconds * IntervalUnits.Minutes * IntervalUnits.Hours
}