interface User {
    id: number,
    first_name: string,
    last_name: string,
    username: string
}

export default interface Bet {
    id: number,
    performer: User,
    bid: number,
    lot: number
}