import ReadOnlyModel from "./ReadOnlyModel";

export default interface City extends ReadOnlyModel {
    title: string
}