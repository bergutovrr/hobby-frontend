import ReadOnlyModel from "./ReadOnlyModel";
import City from "./City";

export default interface Country extends ReadOnlyModel {
    title: string,
    cities?: City[]
}