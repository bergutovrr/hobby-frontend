import * as base64 from '../layers/encoding/base64';

export interface AuthJWTPayload {
    username: string,
    user_id: number,
    email: string,
    exp: number
}

export default class JWTToken<T = any> {

    token: string;

    constructor(token: string) {
        this.token = token;
    }

    get payload(): T & { token: string } {
        const token = this.token;
        const [header, payload, signature] = token.split(".");
        return {...JSON.parse(base64.decode(payload)), token};
    }
}