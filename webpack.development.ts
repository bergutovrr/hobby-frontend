const {CheckerPlugin} = require('awesome-typescript-loader');

import * as config from './config';
import * as webpack from 'webpack';
import plugins from "./webpack/webpack.plugins";
import sharedWebpackConfig from "./webpack.shared";

const ErrorOverlayPlugin = require('error-overlay-webpack-plugin');

const webpackConfig: webpack.Configuration = {
    ...sharedWebpackConfig,
    entry: [
        'webpack-hot-middleware/client',
        config.clientEntry
    ],
    devtool: "eval",

    // optimization: {
    //     noEmitOnErrors: false
    // },

    plugins: [
        ...plugins,
        new webpack.NamedModulesPlugin(),
        new webpack.HotModuleReplacementPlugin(),
        new CheckerPlugin(),
        new ErrorOverlayPlugin()
    ],
    mode: "development"
};

export default webpackConfig;